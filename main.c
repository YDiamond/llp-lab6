#include <stdio.h>
#include "image_utils.h"
#include <string.h>
#include "sepia_c.h"
#include "sepia_sse.h"
#include <sys/time.h>
#include <sys/resource.h>


enum mode{
    M_ROTATE=0,
    M_SEPIA,
    M_SEPIA_SSE
};
long interval_ms(struct timeval start, struct timeval end){
    return (end.tv_sec - start.tv_sec) * 1000000L + end.tv_usec-start.tv_usec;
}
int main(int argc, char* argv[] ) {

    struct rusage r;
    struct timeval r_start, r_end;
    struct timeval u_start, u_end;
    FILE *in = fopen(argv[1], "rb");
    FILE *out = fopen(argv[2], "wb");
    image_t image, *image_res = malloc(sizeof(image_t));
    int angle = 90;
    enum mode mode;



    if(argc < 4){
        fputs("Usage for lab8: input.bmp out.bmp [rotate=<angle>|sepia|sepia_sse]\n", stderr);
        return 2;
    }

    if(!strncmp(argv[3], "rotate", 6)) mode = M_ROTATE;
    else if(!strcmp(argv[3], "sepia")) mode = M_SEPIA;
    else if(!strcmp(argv[3], "sepia_sse")) mode = M_SEPIA_SSE;
    else{
        fputs("Usage for lab8: input.bmp out.bmp [rotate=<angle>|sepia|sepia_sse]\n", stderr);
        return 2;
    }


    if(!in || !out){
        printf("Couldn't open files\n");
        return 1;
    }
    enum read_status read = read_from_bmp(in, &image);
    if(read != READ_OK){
        perror_read("Error while deserializing a bmp file\n", read);
        return 1;
    }

    //show(&image);

    gettimeofday(&r_start, NULL);
    getrusage(RUSAGE_SELF, &r);
    u_start = r.ru_utime;

    switch (mode) {
        case M_ROTATE:{
            sscanf(argv[3],"rotate=%d",&angle);
            printf("angle = %d\n", angle);
            *image_res = rotate(&image,angle);
            break;
        }
        case M_SEPIA:{
            sepia_c(&image, image_res);
            break;
        }
        case M_SEPIA_SSE:{
            sepia_sse(&image, image_res);
            break;
        }
    }

    gettimeofday(&r_end, NULL);
    getrusage(RUSAGE_SELF, &r);
    u_end = r.ru_utime;

    printf("---------------------------------\n");
    //show(image_res);



    enum write_status write = write_to_bmp(out, image_res);
    if(write != WRITE_OK){
        perror_write("Error while serializing a bmp file", write);
        return 1;
    }
    if(fclose(in) || fclose(out)){
        printf("Couldn't close files");
        return 1;
    }

    printf("Total time for performing function:  %ld mcs\n",
           interval_ms(r_start,r_end));

    free(image_res->data);
    free(image.data);
    return 0;
}
