SOURCES_C = main.c image_utils.c sepia_c.c sepia_sse.c
SOURCES_ASM = sepia_asm.asm
OBJS_ASM = sepia_asm.o
OBJS_C = ${SOURCES_C:.c=.o}
EXEC = lab8
OBJS :=$(OBJS_C) $(OBJS_ASM)

GCC = gcc
GCC_FLAGS = -pedantic-errors -Wall -Werror
NASM = nasm
ASMFLAGS = -g -felf64


all:$(EXEC)

$(EXEC) : $(OBJS)
	$(GCC) $^ -lm -o $@

sepia_asm.o: sepia_asm.asm
	$(NASM) $(ASMFLAGS) $^ -o $@

%.o : %.c
	$(GCC) $< -c -o $@

main.c: image_utils.h sepia_c.h sepia_sse.h
clean :
	rm -rf $(OBJS)