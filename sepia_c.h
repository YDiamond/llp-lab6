//
// Created by YDiamond on 15.12.2020.
//

#ifndef LABA6_SEPIA_C_H
#define LABA6_SEPIA_C_H

#include "image_utils.h"
void sepia_c( struct image* img, struct image* res ) ;

#endif //LABA6_SEPIA_C_H
