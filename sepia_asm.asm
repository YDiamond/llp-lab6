global sepia_asm
section .text

sepia_asm:
    mov rcx, 4
    .loop:
        movdqa xmm0, [rdi]
	    movdqa xmm1, [rdi+48]
	    movdqa xmm2, [rdi+96]

	    mulps xmm0, [rsi]
	    mulps xmm1, [rsi+48]
	    mulps xmm2, [rsi+96]

	    addps xmm0, xmm1
    	addps xmm0, xmm2

    	movdqa [rdx], xmm0
        add rdi, 16
    	add rsi, 16
    	add rdx, 16

    	dec rcx
    	test rcx, rcx
    	ja .loop
        ret
