//
// Created by YDiamond on 15.12.2020.
//

#include "sepia_c.h"

static unsigned char sat(uint64_t x) {
    if (x < 256) return x; return 255;
}
static const float c[3][3] = {
        {.393f, .769f, .189f},
        {.349f, .686f, .168f},
        {.272f, .543f, .131f}};
static struct pixel_t pixel_out;
static struct pixel_t sepia_one( struct pixel_t const pixel_in ) {
    pixel_out.r = sat(
            pixel_in.r * c[0][0] + pixel_in.g * c[0][1] + pixel_in.b * c[0][2]
    );
    pixel_out.g = sat(
            pixel_in.r * c[1][0] + pixel_in.g * c[1][1] + pixel_in.b * c[1][2]
    );
    pixel_out.b = sat(
            pixel_in.r * c[2][0] + pixel_in.g * c[2][1] + pixel_in.b * c[2][2]
    );
    return pixel_out;
}

void sepia_c( struct image* img, struct image* res ) {
    uint32_t x,y;
    //if(res == NULL) return;
    res->width = img->width;
    res->height = img->height;
    res->data = malloc(sizeof(struct pixel_t) * res->width * res->height);
    for( y = 0; y < img->height; y++ )
        for( x = 0; x < img->width; x++ ) {
             set_pixel(res, y,x,sepia_one(get_pixel(img, y, x)));
        }
}
