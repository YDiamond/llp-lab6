//
// Created by YDiamond on 15.12.2020.
//

#ifndef LABA6_SEPIA_SSE_H
#define LABA6_SEPIA_SSE_H
#include "image_utils.h"


#define c0_0 .131f
#define c0_1 .543f
#define c0_2 .272f
#define c1_0 .168f
#define c1_1 .686f
#define c1_2 .349f
#define c2_0 .189f
#define c2_1 .769f
#define c2_2 .393f

#define SEPIA_C_COL_0 c0_0, c1_0, c2_0
#define SEPIA_C_COL_1 c0_1, c1_1, c2_1
#define SEPIA_C_COL_2 c0_2, c1_2, c2_2

void sepia_sse (struct image* const img, struct image* const res);

#endif //LABA6_SEPIA_SSE_H
