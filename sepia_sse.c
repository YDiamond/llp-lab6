//
// Created by YDiamond on 15.12.2020.
//

#include "sepia_sse.h"

extern void sepia_asm(float* input, float* consts, float* output);

static uint8_t sat(uint64_t x) {
    if (x < 256) return x; return 255;
}

static float consts[36] = {
    SEPIA_C_COL_0, SEPIA_C_COL_0, SEPIA_C_COL_0, SEPIA_C_COL_0,
    SEPIA_C_COL_1, SEPIA_C_COL_1, SEPIA_C_COL_1, SEPIA_C_COL_1,
    SEPIA_C_COL_2, SEPIA_C_COL_2, SEPIA_C_COL_2, SEPIA_C_COL_2
};

static struct pixel_t sepia_one( struct pixel_t const pixel_in ) {
    static const float c[3][3] = {
            {.393f, .769f, .189f},
            {.349f, .686f, .168f},
            {.272f, .543f, .131f}};
    struct pixel_t pixel_out;
    pixel_out.r = sat(
            pixel_in.r * c[0][0] + pixel_in.g * c[0][1] + pixel_in.b * c[0][2]
    );
    pixel_out.g = sat(
            pixel_in.r * c[1][0] + pixel_in.g * c[1][1] + pixel_in.b * c[1][2]
    );
    pixel_out.b = sat(
            pixel_in.r * c[2][0] + pixel_in.g * c[2][1] + pixel_in.b * c[2][2]
    );
    return pixel_out;
}

void sepia_sse (struct image* const img, struct image* const res){
    if(res == NULL) return;
    res->width = img->width;
    res->height = img->height;
    res->data = malloc(img->height * img->width * sizeof(struct pixel_t));


    struct pixel_t *data = img->data;
    uint8_t *g_result = (uint8_t*)res->data;
    uint64_t blocks_count = img->height * img->width / 4;
    uint64_t i;
    float packed_pixels[36];



    for(i = 0; i < blocks_count; i++){
        float result_mul[12];
        uint64_t j;
        for(j = 0; j < 12; j+=3){
            packed_pixels[j] = packed_pixels[j+1] = packed_pixels[j+2] = data->b;
            packed_pixels[j+12] = packed_pixels[j+13] = packed_pixels[j+14] = data->g;
            packed_pixels[j+24] = packed_pixels[j+25] = packed_pixels[j+26] = data->r;
            data++;
        }

        sepia_asm(packed_pixels, consts, result_mul);
        for(j = 0; j < 12; j++){
            *g_result++ = sat((uint64_t)roundf(result_mul[j]));
        }
    }
    printf("ended sepia_asm\n");
    for (i = blocks_count * 4; i < img->width * img ->height; ++i) {
        res->data[i] = sepia_one(img->data[i]);
    }
}